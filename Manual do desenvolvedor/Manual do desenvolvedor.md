# Manual do desenvolvedor
 >Este manual é destinado aos desenvolvedores do projeto.
### Sumário
 1-Introdução
 1.1-Público Alvo
 1.2-Pré-requisitos técnicos
 
 2-Habilitar a plataforma de execução
 
 3- Iniciando o servidor para testes

 4-Lista de Mensagens Suportadas

* JOIN
* LEAVE
* PRIMVSG
* NICK
* QUIT