const _APP = require('net');
const _SERVER = require('./server/server.js');
const _PORT = 6667; // porta padrão para o servidor receber as requisições

// Cria servidor
_APP.createServer(socket => {
    _SERVER(socket);
}).listen(_PORT);

console.log("Chat server running at port " + _PORT + "\n");