/**
 * @file: clients.js 
 * @description: Funções relacionadas ao cliente
*/

const _ROOMS = require('../server/rooms/rooms');
const _CLIENTS = []; //vetor de sockets
const _CLIENT = {}; //objeto vazio client
const _NICKS = {};

// Adiciona Clientes
_CLIENT.addClient = (socket) => {
    _CLIENTS.push(socket);
};

// retorna um array de clients (array de sockets)
_CLIENT.getClients = () => {
    return _CLIENTS;
}

// retorna um array com os nomes de usuários
_CLIENT.getNicks = () => {
    return _NICKS;
}

/**
 * @description Atribui a propriedade name do socket à constante _NICKS nomeando a posição com "nick"
 * @param nick - nome da posição do array que terá como valor o parametro "name"
 * @param name - propriedade name do socket
*/
_CLIENT.bindNicksName = (nick, name) => {
    _NICKS[nick] = name;
}

// verifica existência do nick
_CLIENT.verifyNick = (nick) => {
    return (_NICKS[nick]) ? true : false;
}

// @description remove o objeto socket da lista de CLIENTS 
_CLIENT.removeRoomMain = (socket) => {
    _CLIENTS.splice(_CLIENTS.indexOf(socket), 1);
}

/**
 * @description remove o objeto socket da lista de CLIENTS 
 * @description remove a propriedade nick do objeto socket da lista de NICKS
 * */
_CLIENT.removeClient = (socket) => {
    _CLIENT.removeRoomMain(socket);
    delete _NICKS[socket.nick];
}

// realiza o broadcast das mensagens
_CLIENT.broadcast = (message, sender) => {
    _CLIENTS.forEach(client => {
        if (client === sender) return;
        client.write(message);
    });
    process.stdout.write(message);
}

/**
 * @description verifica na listagem de names se existe o nick, caso não exista
 *      verfica na salas se existe o nick para disparar a mensagem
 * @param nick alvo que receberá a mensagem
 * @param msg mensagem que será enviada ao alvo
 */
_CLIENT.findSocketByNickPRIVMSG = (nick, msg) => {
    _CLIENTS.forEach(client => {
        if (client.nick == nick)
            client.write(msg)
    });
    ROOMS.getRooms().forEach(room => {
        room.clients.forEach(client => {
            if (client.nick == nick)
                client.write(msg)
        })
    })
}

module.exports = _CLIENT;