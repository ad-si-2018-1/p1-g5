/**
 * Funções relacionadas ao servidor
 */
//fazendo uma requisição para usar as funções de cliente
const _CLIENTS = require('../clients/clients');
const _COMMANDS = require('../commands/commands');
const _ROOM = require('../server/rooms/rooms');
const _LOG = require('../server/log/log');
const _OBJ_SERVER = require('../server/Obj_Server');

// período de espera pelo PONG 5 minutos
let timeoutPeriod = 5 * 60 * 1000;

// utilizado para reiniciar o timeout
let timeout = null; 

// criando dados do servidor;
_OBJ_SERVER.create({
    name    : "SI-AD-IRC",
    admin   : "G5",
    version : "P1_1.0",
    created : _COMMANDS.getCurrentDate()
});

//Monitorar o status do socket
module.exports = socket => {
    // em 5 minutos irá chamar o onNoPong e emitirá mensagem timeout
    let onSilence = () => { 
        timeout = setTimeout(onNoPong, timeoutPeriod);
    };
    let onNoPong = () => {
        socket.emit('timeout');
    };

    socket.name = socket.remoteAddress + ":" + socket.remotePort;
    socket.write("Bem vindo " + socket.name + "\n");
    message = socket.name + " entrou no sala \n";

    _CLIENTS.broadcast(message, socket);
    _LOG(message);

    _CLIENTS.addClient(socket);

//Quando o cliente envia dados ao server
    socket.on('data', data => {
        message = socket.nick + "> " + data
        
        //não faz broadcast de comandos executados
        if (!analiseData(data, socket) && socket.nick) {
            if (_CLIENTS.getClients().indexOf(socket) == -1) {
                _ROOM.getRooms().forEach(elem => {
                    if (elem.clients.indexOf(socket) != -1)
                        _ROOM.broadcastRoom(message, socket, elem.name);
                })
            }
            else {
                _CLIENTS.broadcast(message, socket);
                _LOG(message);
            }
        }

        //recebemos dados reseta o timeout
        //mensagens recebidas são o PONG
        clearTimeout(timeout);
        timeout = setTimeout(onSilence, timeoutPeriod);
    });

    socket.on('end', msg => {
        if (msg) {
            message = msg + " saiu da sala. \n";
        } else {
            message = socket.nick + " foi expulso por inatividade \n"
            socket.write(message);
        }
        _CLIENTS.removeClient(socket);
        socket.destroy();
        process.stdout.write(message);
        _LOG(message);
    });

    socket.on('error', () => {
        if (socket.nick)
            message = "ERROR 101 - [SOCKET]: " + socket.nick + "> foi destruído \n"
        else
            message = "ERROR 101 - [SOCKET]: " + socket.name + "> foi destruído \n"

        process.stdout.write(message);
        socket.destroy();
        _LOG(message);
    });

    //Se detectar mensagem do socket de timeout encerra ele
    socket.on('timeout', () => {
        socket.emit('end');
    });
}

function analiseData(data, socket) {
    //método trim remove os caracteres especiais no final ad string
    let message = String(data).trim();
    let args = message.split(" ");
    if (args[0] != "NICK" && socket.nick == undefined) {
        socket.write("WARNING - É NECESSÁRIO EXECUTAR O COMANDO: NICK <name> \n");
    } else {
        if (args[0] == "NICK") {
            _COMMANDS.nick(args, socket);
            return true
        } // se o primeiro argumento for NICK
        else if (args[0] == "QUIT") {
            _COMMANDS.quit(socket);
            return true
        }
        else if (args[0] == "JOIN") {
            if (args[1]){
                _COMMANDS.join(args[1], socket);
                _CLIENTS.removeRoomMain(socket);
                return true;
             } else {
                 socket.write("ERRO 201 [JOIN] - Necessário informar um parametro após o comando 'JOIN' \n");
             };
        }
        else if (args[0] == "LEAVE") {
            if (args[1]){
                _COMMANDS.leaveRoom(args[1], socket);
                return true;
             } else {
                 socket.write("ERRO 201 [LEAVE] - Necessário informar um parametro após o comando 'LEAVE' \n");
             };
        }
        else if (args[0] == "PRIVMSG") {
            if (args[1]){
                _COMMANDS.privmsg(data, args[1], socket);
                return true;
             } else {
                 socket.write("ERRO 201 [PRIVMSG] - Necessário informar um parametro após o comando 'PRIVMSG' \n");
             };
        }
        else if (args[0] == "USERS") {
            _COMMANDS.users(socket);
            return true;
        }
        else if (args[0] == "LIST") {
            _COMMANDS.list(socket);
            return true;
        }
        else if (args[0] == "ROOMS") {
            _COMMANDS.rooms(socket);
            return true;
        }
        else if (args[0] == "TIME") {
            _COMMANDS.time(socket);
            return true;
        }
        else if (args[0] == "INFO") {
            _COMMANDS.info(socket);
            return true;
        }
    }
    return false;
}