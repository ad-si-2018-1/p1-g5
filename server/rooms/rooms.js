// ------------------------------------------------------------- //
//                 Funções relacionadas à sala de batepapo       //
// ------------------------------------------------------------- //

const LOG = require('../log/log'),
    CLIENTS = require('../../clients/clients'),
    ROOMS = [],// array de salas
    funcRoom = {};
let message = " ";

funcRoom.verifyRoom = (name, socket) => {
    let numRoom = getIdRoom(name);
    if (numRoom > -1) {
        joinRoom(numRoom, socket);
    } else {
        createRoom(name, socket);
    }

}

funcRoom.leaveRoom = (name, socket) => {
    let index = getIdRoom(name);
    if (index != -1) {
        let indCliente = ROOMS[index].clients.indexOf(socket);
        socket.write("YOU LEFT THE ROOM " + name + "\n");
        message = socket.nick + " LEFT THE ROOM \n";

        funcRoom.broadcastRoom(message, socket, name);
        ROOMS[index].clients.splice(indCliente, 1);
        //se existir outra sala com esse cliente retorna falso,
        //para não ser incluido na sala main
        //só será colocado na mais se não estiver em outra sala
        return (getRoomsClient(socket) == 0) ? true : false;
    } else {
        message = "ERRO: Voce nao esta na sala " + name + "\n";
        socket.write(message);
        return false;
    }

}

funcRoom.broadcastRoom = (data, sender, room) => {
    let numRoom = getIdRoom(room);

    if (numRoom > -1) {
        ROOMS[numRoom].clients.forEach(client => {
            message = ROOMS[numRoom].name + " >> " + data;
            if (client === sender) return;
            client.write(message);
        });
        process.stdout.write(message);
        LOG("ROOM: " + message + "\n");

    } else {
        message = "ERRO: Tentando enviar mensagem para sala inexistente\n"
        process.stdout.write(message + ":::" + room);
        sender.write(message);
    }
}

funcRoom.getRooms = () => {
    return ROOMS;
}

module.exports = funcRoom;

function joinRoom(i, socket) {

    if (ROOMS[i].clients.indexOf(socket) == 1) {
        socket.write("VOCE JA ESTA NA SALA " + ROOMS[i].name + "\n");
    } else {

        message = socket.nick + " JOINED THE ROOM\n";
        funcRoom.broadcastRoom(message, socket, ROOMS[i].name);

        ROOMS[i].clients.push(socket);
        socket.write("YOU ARE IN " + ROOMS[i].name + "\n");

    }
}

function createRoom(name, socket) {
    let room = {
        name: "",
        clients: []
    };//Obj JSON sala

    room.name = name;
    room.clients.push(socket)

    ROOMS.push(room);
    socket.write("YOU CREATE ROOM " + name + "\n");
    message = socket.nick + " CREATE ROOM " + name + "\n";
    process.stdout.write(message);
    LOG(message);
}

function getIdRoom(name) {
    let index = -1;
    ROOMS.forEach((elem, i) => {
        if (elem.name == name) {
            return index = i;
        }
    });
    return index;
}

function getRoomsClient(socket) {
    let find = 0;
    ROOMS.forEach(elem => {
        if (elem.clients.indexOf(socket) != -1)
            find++;
    });
    return find;
}