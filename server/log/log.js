const _FS = require('fs');

let date = new Date();
let day = date.getDate();
let month = (date.getMonth() + 1);
let year = date.getFullYear();
let formattedDate = day + '-' + month + '-' + year;

const _PATHLOG = './server/log/' + formattedDate + '.log.txt';

module.exports = (message) => {
    //flag a - se arquivo existir ele edita, se não existir ele cria um novo
    _FS.writeFile(_PATHLOG, message+"\r\n", { enconding: 'utf-8', flag: 'a' }, err => {
        if (err) process.stdout.write(err);
    });
}