const _SERVER = {};

let obj_server = {
    name    : "",
    admin   : "",
    version : "",
    created : ""
}

_SERVER.create = (obj) => {
    obj_server.name     = obj.name;
    obj_server.admin    = obj.admin;
    obj_server.version  = obj.version;
    obj_server.created  = obj.created;
}

_SERVER.toString = () =>{
    let obj =   "* Server name: "     + obj_server.name
              + "\n* Admin: "        + obj_server.admin
              + "\n* Version: "      + obj_server.version
              + "\n* Created on: "   + obj_server.created;

    return obj;
}
module.exports = _SERVER;