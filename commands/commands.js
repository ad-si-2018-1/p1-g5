const _CLIENTS = require('../clients/clients');
const _ROOMS = require('../server/rooms/rooms');
const _LOG = require('../server/log/log');
const _SERVER = require('../server/server');
const _OBJ_SERVER = require("../server/Obj_Server")
const _COMMANDS = {};

let message = " ";

//args é um vetor com o comando digitado pelo cliente, sendo args[0] = "NICK"
_COMMANDS.nick = (args, socket) => {
    let nicks = _CLIENTS.getNicks();

    if (!args[1]) {
        socket.write("ERROR: NICK IS EMPTY\n");
        return;
    }
    else {
        args[1] = args[1].toUpperCase();
    }
    if (_CLIENTS.verifyNick(args[1])) {
        socket.write("ERROR: NICKNAME ALREADY EXISTS\n");
        return;
    }

    //no vetor nick associa o nick ao name do socket
    _CLIENTS.bindNicksName(args[1], socket.name);
    socket.nick = args[1];

    socket.write("NEW NICKNAME: " + socket.nick + "\n");
    message = socket.name + " NOW IS " + socket.nick + "\n";
    _CLIENTS.broadcast(message, socket);
    _LOG(message);
}

_COMMANDS.quit = (socket) => {
    socket.emit('end', socket.nick); //Socket envia mensagem ao servidor
}

_COMMANDS.join = (param, socket) => {
    _ROOMS.verifyRoom(param, socket);
}

_COMMANDS.leaveRoom = (name, socket) => {
    if (_ROOMS.leaveRoom(name, socket)) //ao retirar o socket da(s) salas 
        _CLIENTS.addClient(socket);     // adiciona ele ao vetor clients novamente
};

_COMMANDS.privmsg = (data, dest, sender) => {
    let message = String(data).trim();
    let args = message.split("PRIVMSG " + dest);

    if (_CLIENTS.verifyNick(dest)) {
        message = sender.nick + " to " + dest + " : " + args[1] + "\n";
        _CLIENTS.findSocketByNickPRIVMSG(dest, message);
        _LOG("PRIVMSG: " + message)
    } else {
        sender.write("USER " + dest + " DOESN'T EXISTS");
    }
};

_COMMANDS.users = (socket) => {

    message = "";

    for (let i in _CLIENTS.getNicks()) {
        message += i + "\n";
    }

    socket.write(message);
    message = socket.nick + " EXECUTED COMMAND USERS\n";
    _LOG(message);
    process.stdout.write(message);
}

_COMMANDS.list = (socket) => {
    message = "";
    _ROOMS.getRooms().forEach(room => {
        message += room.name + "\n";
    });

    if (message == "")
        message = "WARNING: Não há salas disponíveis! \n";

    socket.write(message);
    message = socket.nick + " EXECUTED COMMAND LIST\n";
    _LOG(message);
    process.stdout.write(message);
}

_COMMANDS.rooms = (socket) => {
    message = "";

    _ROOMS.getRooms().forEach(room => {
        if (room.clients.indexOf(socket) > -1)
            message += room.name + "\n"
    });

    if (message == "")
        message = "MAIN\n";

    socket.write(message);
    message = socket.nick + " EXECUTED COMMAND ROOMS\n";
    _LOG(message);
    process.stdout.write(message);
}

_COMMANDS.time = (socket) => {
    let months = [
        "JAN",
        "FEV",
        "MAR",
        "ABR",
        "MAI",
        "JUN",
        "JUL",
        "AGO",
        "SET",
        "OUT",
        "NOV",
        "DEZ"
    ],
        date = new Date(),
        hour = date.getHours(),
        minute = date.getMinutes(),
        seconds = date.getSeconds();
        currentDateArr = _COMMANDS.getCurrentDate().split('/');
        currentDate = currentDateArr[0] + '/' + months[currentDateArr[1]] + '/' + currentDateArr[2];
        
    message = "SERVER HOUR: (" + currentDate + "): ";
    message += hour + "h" + minute + "m" + seconds + "s \n";

    socket.write(message);
    message = socket.nick + " EXECUTED COMMAND TIME \n";
    _LOG(message);
    process.stdout.write(message);
}

_COMMANDS.info = (socket) => {
    socket.write(_OBJ_SERVER.toString());
    message = socket.nick + " EXECUTED COMMAND INFO \n";
    _LOG(message);
    process.stdout.write(message);
}

_COMMANDS.getCurrentDate = () => {
    var currentDate = new Date();
    var dd = currentDate.getDate();
    var mm = currentDate.getMonth();
    var yyyy = currentDate.getFullYear();

    currentDate = dd + '/' + mm + '/' + yyyy;

    return currentDate;
}
module.exports = _COMMANDS;