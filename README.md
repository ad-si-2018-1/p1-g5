# PROJETO 1 DA DISCIPLINA DE APLICAÇÕES DISTRIBUÍDAS                                                   
                                                                                                       
## MEMBROS                                                                                             
                                                                                                       
* Guilherme Paniago - guilhermepaniago@hotmail.com
* Guilherme Quirino - guilherme_quirino@hotmail.com (Desenvolvedor)
* Daniel Cavalcante - daniel56_cavalcante@hotmail.com
* Gilmar Alves Bernardes Júnior - gilbernardes@outlook.com

---

* Dentro da pasta do projeto
    
    ```
    npm install
    ```
* Executando o projeto
    
    ```
    npm run irc
    ```
---

1. Funções implementadas
    - Operação de canal
        * JOIN

            ```
            JOIN <nome_do_canal>
            ```
        * LEAVE 
            ```
             LEAVE <nome_do_canal>
            ```
    - PRIVMSG (mensagem privada)
    
        ```
        PRIVMSG <NICKNAME> <mensagem>
        ```
    - Variadas 
        * NICK (Alterando o nickname, caso não tenha o nickname somente o name é apresentado)

            ```
            NICK <nickname>
            ```
        * QUIT (KILL - deixa o chat)
            
            ```
            QUIT
            ```
        * PING - se o cliente ficar 5 minutos inativo será encerrado
        * PONG - a cada dado recebido pelo servidor o tempo é reiniciado
        * ERROR - quando um 'FATAL ERROR' acontece, o socket que emitiu o erro é retirado do server
    - USER BASED QUERIES 
        - USERS (mostra todos os usuários conectados)

            ```
            USERS
            ```
        - LIST (Listar os canais)
    
            ```
            LIST
            ```
        - ROOMS (Listar os canais que o cliente esta conectado)
        
            ```
            ROOMS
            ```
    - Server queries and commands (https://tools.ietf.org/html/rfc2812#section-3.4)
        - TIME - retorna data e hora do server
        
            ```
            TIME
            ```

2. Faltam implementar
    - NOTICE ( https://tools.ietf.org/html/rfc2812#section-3.3.2 )
    